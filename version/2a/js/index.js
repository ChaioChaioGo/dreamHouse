window.addEventListener('DOMContentLoaded', function(e) {
    // DOM Tree 載完
    // console.log('onDOMContentLoaded');
    // 新的容器 用於放 房間照片slider 和 環境照片slider
    var divC = document.createElement('div'),
        div1 = document.createElement('div'),
        div2 = document.createElement('div'),
        tab = document.createElement('div');
        btn1_prev = document.createElement('div');
        btn1_next = document.createElement('div');
    var main = document.getElementById('main'),
        header = document.getElementById('header'),
        bnbIntroPhotosSlider = document.getElementById('bnbIntroPhotosSlider'),
        bnbRoomsSlider = document.getElementById('bnbRoomsSlider');
    // 安裝新的slider容器
    divC.id = 'twoSliderContainer';
    divC.className = 'two-slider-container';
    div1.id = 'slider1';
    div1.className = 'slider-container-1 js-s-c-1 active';
    div2.id = 'slider2';
    div2.className = 'slider-container-2 js-s-c-2';

    div1.appendChild(bnbRoomsSlider);
    // 按鈕
    if(bnbRoomsSlider.querySelectorAll('img').length > 1) {
        btn1_prev.id = 'btn1Prev';
        btn1_prev.className = 'btn-owlnav-big btn-prev-big';
        btn1_next.id = 'btn1Next';
        btn1_next.className = 'btn-owlnav-big btn-next-big';
        div1.appendChild(btn1_prev);
        div1.appendChild(btn1_next);
    };
    
    div2.appendChild(bnbIntroPhotosSlider);
    divC.appendChild(div1);
    divC.appendChild(div2);
    document.getElementById('form1').insertBefore(divC, main);
    // 加上 Room標籤和 Album標籤
    // 安裝 tab
    tab.id = 'tabDiv';
    tab.className = 'tab-div';
    tab.innerHTML = '<a href="javascript:;" id="roomTab" class="slider-tab tab-1 js-tab-1 active">Room</a><a href="javascript:;" id="albumTab" class="slider-tab tab-2 js-tab-2">Album</a>';
    header.appendChild(tab);

    // 更換按鈕的文字
    document.getElementById('promoMoreBtn').textContent = 'more';
    document.getElementById('newsMoreBtn').textContent = 'more';

    // 欲刪除的元件
    var indexNone = [
        'sliderSect'
    ];

    // 刪除 DOM
    Tools.delDOM(indexNone);

    indexInit(); // 共用func
});

var indexInit = function() { // 共用func
    var owlInited = false;
    $('.bnb-intro-photos').owlCarousel({
        loop: multiPhotos(),
        autoplay: true,
        center: true,
        lazyLoad: true,
        lazyLoadEager: 1,
        dots: false,
        autoHeight: true,
        nav: false,
        responsive: {
            0: {
                items: 1,
                margin: 0,
                autoplaySpeed: 300,
                autoplayTimeout: 2000
            },
            // breakpoint from 768 up
            768: {
                items: 1,
                margin: 0,
                autoplaySpeed: 300,
                autoplayTimeout: 2000
            },
            // breakpoint from 999 up
            999: {
                items: 1,
                margin: 5,
                autoplaySpeed: 300,
                autoplayTimeout: 2000,
                stagePadding: 300
            },
            // breakpoint from 1199 up
            1199: {
                items: 3,
                margin: 5,
                autoplaySpeed: 300,
                autoplayTimeout: 2000
            },
            // breakpoint from 1399 up
            1399: {
                items: 3,
                margin: 5,
                autoplaySpeed: 300,
                autoplayTimeout: 2000
            }
        }
        
    }).on('resized.owl.carousel', function () {
        /* 修正windows resize 後 autoHeight 不會自動更新 */
        $(this).data('owlCarousel')._plugins.autoHeight.update();
    });
    
    // 2019-06-17 update
    var owlRoomTypeObj = defineOwlRoomTypeObj();
    var owl1 = $('.bnb-rooms').owlCarousel({
        autoplay: false,
        lazyLoad: true,
        lazyLoadEager: 1,
        dots: false,
        autoHeight: true,
        navText: ['<div class="arrow"></div>', '<div class="arrow"></div>'],
        responsive: {
            0: {
                loop: owlRoomTypeObj.narrowLoop,
                center: owlRoomTypeObj.narrowCenter,
                nav: owlRoomTypeObj.narrowNav,
                items: 1,
                margin: 0
            },
            // breakpoint from 768 up
            768: {
                loop: owlRoomTypeObj.narrowLoop,
                center: owlRoomTypeObj.narrowCenter,
                nav: owlRoomTypeObj.narrowNav,
                items: 1,
                margin: 0
            },
            // breakpoint from 999 up
            999: {
                loop: owlRoomTypeObj.midLoop,
                center: owlRoomTypeObj.midCenter,
                nav: owlRoomTypeObj.midNav,
                items: owlRoomTypeObj.midItems,
                margin: 5,
                stagePadding: owlRoomTypeObj.midStagePadding
            },
            // breakpoint from 1199 up
            1199: {
                loop: owlRoomTypeObj.wideLoop,
                center: owlRoomTypeObj.wideCenter,
                nav: owlRoomTypeObj.wideNav,
                items: 3,
                margin: 5
            },
            // breakpoint from 1399 up
            1399: {
                loop: owlRoomTypeObj.wideLoop,
                center: owlRoomTypeObj.wideCenter,
                nav: owlRoomTypeObj.wideNav,
                items: 3,
                margin: 5
            }
        }
        
    }).on('resized.owl.carousel', function () {
        /* 修正windows resize 後 autoHeight 不會自動更新 */
        $(this).data('owlCarousel')._plugins.autoHeight.update();
    });

    function defineOwlRoomTypeObj() {
        var nums = $('.bnb-rooms').find('.img-item').length;
        var obj = {};
        switch (nums) {
            // case 3:
            //     $('.room-link-blk').addClass('active');
            //     $('.btn-owlnav-big').addClass('few-3');
            //     obj.narrowLoop = true;
            //     obj.narrowCenter = true;
            //     obj.narrowNav = true;
            //     obj.midLoop = false;
            //     obj.midCenter = false;
            //     obj.midNav = false;
            //     obj.midItems = 3;
            //     obj.midStagePadding = 0;
            //     obj.wideLoop = false;
            //     obj.wideCenter = false;
            //     obj.wideNav = false;
            //     // console.log('3');
            //     break;

            case 2:
                $('.room-link-blk').addClass('active');
                $('.btn-owlnav-big').addClass('few-2');
                obj.narrowLoop = true;
                obj.narrowCenter = true;
                obj.narrowNav = true;
                obj.midLoop = false;
                obj.midCenter = false;
                obj.midNav = false;
                obj.midItems = 2;
                obj.midStagePadding = 0;
                obj.wideLoop = false;
                obj.wideCenter = false;
                obj.wideNav = false;
                // console.log('2');
                break;

            case 1:
                // 只有一個房間時，幫黑毛加上active
                $('.room-link-blk').addClass('active');
                obj.narrowLoop = false;
                obj.narrowCenter = false;
                obj.narrowNav = false;
                obj.midLoop = false;
                obj.midCenter = false;
                obj.midNav = false;
                obj.midItems = 1;
                obj.midStagePadding = 300;
                obj.wideLoop = false;
                obj.wideCenter = false;
                obj.wideNav = false;
                // console.log('1');
                break;

            default:
                obj.narrowLoop = true;
                obj.narrowCenter = true;
                obj.narrowNav = true;
                obj.midLoop = true;
                obj.midCenter = true;
                obj.midNav = true;
                obj.midItems = 1;
                obj.midStagePadding = 300;
                obj.wideLoop = true;
                obj.wideCenter = true;
                obj.wideNav = true;
                // console.log('3up');
        }

        return obj;
    };

    $('#btn1Prev').on('click', function(e) {
        owl1.trigger('prev.owl.carousel');
    });
    $('#btn1Next').on('click', function(e) {
        owl1.trigger('next.owl.carousel');
    });

    $(window).load(function () {
        $('.bnb-intro-photos').each(function () {
            $(this).data('owlCarousel')._plugins.autoHeight.update();
        });
        $('.bnb-rooms').each(function () {
            $(this).data('owlCarousel')._plugins.autoHeight.update();
        });

        $('.test-rooms').each(function () {
            $(this).data('owlCarousel')._plugins.autoHeight.update();
        });
    });
    var $win = $(window);
    var owlStatus = false;
    var $lb_box = $('.lightbox-inner-box');
    var $lb = $('#lightBox');
    //為了減輕網頁負擔，等到使用者點擊照片時，再建立lightbox
    $('.js-item').on('click', function(e) {
        e.preventDefault();
        //第一次啟動要建立lightBox的內容
        if (!owlStatus) {
            var picSrc = '', imgElm = '';
            var elm = document.getElementById('bnbIntroPhotosSlider').querySelectorAll('.owl-lazy');
            var n = elm.length;
            for (var i = 0; i < n; i++) {
                picSrc = elm[i].getAttribute('data-src');
                picAlt = elm[i].getAttribute('alt');
                imgElm += '<div class="box-img"><img class="owl-lazy" data-src="' + picSrc + '" alt="' + picAlt + '"></div>';
            };
            $lb_box.append(imgElm);
            //如果相簿只有一張圖就不能把loop打開，另外方向鍵也可以不用給了!
            if ($lb_box.find('.box-img').length > 1) {
                $lb_box.owlCarousel({
                    items: 1,
                    margin: 0,
                    loop: true,
                    lazyLoad: true,
                    nav: true,
                    navText: ['<div class="arrow-btn"></div>', '<div class="arrow-btn"></div>']
                });
            } else {
                $lb_box.owlCarousel({
                    items: 1,
                    margin: 0,
                    lazyLoad: true
                });
            };
            owlStatus = true;
        };
        var triIdx = $(this).index('.js-item');
        $lb_box.trigger('to.owl.carousel', [triIdx, 'no']);
        $lb.addClass('active');
    });

    $('#mask, .close-lb-btn').on('click', function(e) {
        e.preventDefault();
        $lb.removeClass('active');
    });

    // tab click
    var $tab1 = $('.js-tab-1');
    var $tab2 = $('.js-tab-2');
    var $slider1 = $('.js-s-c-1');
    var $slider2 = $('.js-s-c-2');
    $tab1.on('click', function(event) {
        event.preventDefault();
        /* Act on the event */;
        if(window.innerWidth > 767) {
            $('body, html').stop(true, false).animate({scrollTop: window.innerHeight - 210 }, 1000);
        } else {
            $('body, html').stop(true, false).animate({scrollTop: window.innerHeight - 72 }, 1000);
        };
        $tab1.addClass('active');
        $tab2.removeClass('active');
        $slider1.addClass('active');
    });
    $tab2.on('click', function(event) {
        event.preventDefault();
        /* Act on the event */;
        if(window.innerWidth > 767) {
            $('body, html').stop(true, false).animate({scrollTop: window.innerHeight - 210 }, 1000);
        } else {
            $('body, html').stop(true, false).animate({scrollTop: window.innerHeight - 72 }, 1000);
        };
        $tab2.addClass('active');
        $tab1.removeClass('active');
        $slider1.removeClass('active');
    });

    setArticleHeight();

    function multiPhotos() {
        if ($('.bnb-intro-photos').find('.img-item').length > 1) {
            return true;
        } else {
            return false;
        };
    };

    function setArticleHeight() { // 2a 的按鈕位置跟1a不太一樣
        var keepHeight = 168; /* 16 * 1.5 = 24 * n 行 */
        // 為了區分 優惠訊息、最新消息(共用) 的按鈕 所以首頁的 js-unfold-div 變成 js-news-unfold-div
        // news.aspx 則保持 js-unfold-div
        // 2018-09-26 取消此功能
        // $('.js-news-unfold-div').each(function () {
        //     $(this).css({
        //         height: keepHeight +  'px',
        //         overflow: 'hidden',
        //     });
        //     var amHeight = $(this).children('.news-arti-cont').height();
        //     if (amHeight > keepHeight) {
        //         //加上展開的觸發按鈕
        //         $(this).after('<a href="javascript:;" class="news-unfold-btn js-unfold-btn"></a>');
        //     } else {
        //         $(this).css({height: 'auto', paddingBottom: '0'});
        //     };
        // });
        $('.js-unfold-div').each(function () {
            $(this).css({
                height: keepHeight + 'px',
                overflow: 'hidden',
                paddingBottom: '2rem'
            });
            var amHeight = $(this).children('.js-index-unfold-inner').height();
            if (amHeight > keepHeight) {
                //加上展開的觸發按鈕
                $(this).parent().append('<a href="javascript:;" class="index-unfold-btn js-unfold-btn"></a>');
            } else {
                $(this).css({height: 'auto', paddingBottom: '0'});
            };
        });
        // 註冊長篇幅文章展開
        $('.js-unfold-btn').on('click', function () { // 這個按鈕要呼叫不一樣的東西
            var $unfoldDiv = $(this).parent().children('.js-unfold-div');
            if($unfoldDiv.length === 0) {
                $unfoldDiv = $(this).parent().children('.js-news-unfold-div');
            };
            $unfoldDiv.css({height: 'auto', paddingBottom: '0'});
            $(this).remove();
        });

    };

};