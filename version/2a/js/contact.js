window.addEventListener('DOMContentLoaded', function(e) {
    // DOM Tree 載完
    // console.log('onDOMContentLoaded');

    contactInit(); // 共用func
});

var contactInit = function() {
    // solve refresh bug
    document.getElementById('firstRoomSelect').value = '請選擇';

    window.addEventListener('load', function(e) {
        // 捲動到標題
        setTimeout(function() {
            // p: main => navContainer
            // m: contactSect => header
            mobileSetWinowTopPostion('main', 'contactSect');
        }, 0);
    });

    //偽select
    $('.fake-select, .fake-triangle').on('click', function(e) {
        $(this).siblings('.fake-option-list').toggleClass('active');
        e.stopPropagation(); //可以停止事件冒泡傳遞到上層物件
    });
    $('.fake-option-list li').on('click', function(e) {
        var targetVal = $(this).data('val');
        var targetTxt = $(this).text();
        var $targetMarkUp = $(this).parent('.fake-option-list').prev('.fake-select');
        $targetMarkUp.val(targetTxt);
        $targetMarkUp.data('val', targetVal);
        $(this).parent('.fake-option-list').removeClass('active');
        e.stopPropagation(); //可以停止事件冒泡傳遞到上層物件
    });

    $('body').on('click', function() {
        $('.fake-option-list, .fake-option-list-more').removeClass('active');
    });

    $('.add-room-btn').on('click', function() {
        $(moreRoomInput).appendTo('.reserve-room-table');
        var $controlTr = $('.reserve-room-table tr').last();
        //偽select
        $controlTr.find('.fake-select-more, .fake-triangle-more').on('click', function(event) {
            $(this).siblings('.fake-option-list-more').toggleClass('active');
            event.stopPropagation(); //可以停止事件冒泡傳遞到上層物件
        });
        $controlTr.find('.fake-option-list-more li').on('click', function(event) {
            var targetVal = $(this).data('val');
            var targetTxt = $(this).text();
            var $targetMarkUp = $(this).parent('.fake-option-list-more').prev('.fake-select-more');
            $targetMarkUp.val(targetTxt);
            $targetMarkUp.data('val', targetVal);
            $(this).parent('.fake-option-list-more').removeClass('active');
            event.stopPropagation(); //可以停止事件冒泡傳遞到上層物件
        });
        $controlTr.find('.del-btn').on('click', function() {
            $(this).parents('.more-room-tr').remove();
        });
    });

    // 產生 qr-code 圖
    document.getElementById('qrWeb').src = qrCode(location.href.replace(/contact/g, 'index'));

    function qrCode(url) {
        var content = encodeURIComponent(url);
        // return 'https://chart.googleapis.com/chart?cht=qr&chl=' + content + '&chs=140x140&chld=Q|0';
        return 'https://api.qrserver.com/v1/create-qr-code/?data=' + content + '&size=120x120';

    };

    // 產生月曆
    /*JQ-ui-datepicker*/
    /* --datePicker的預設值-- */
    $.datepicker.setDefaults({
        closeText: '關閉',
        prevText: '&#x3C;上月',
        nextText: '下月&#x3E;',
        currentText: '今天',
        monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
        dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
        dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
        dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
        weekHeader: '周',
        dateFormat: 'yy/mm/dd',
        firstDay: 7,
        isRTL: false,
        showMonthAfterYear: true,
        defaultDate: '+0d',
        changeMonth: false,
        numberOfMonths: 1 //一次秀幾個月
    });
    $('#txtCheckinDate').datepicker({
        minDate: '+0d'
    });
    $('#txtRemitAt').datepicker();

};

/*檢驗表單*/
function checkForm() {
    // 定義錯誤訊息
    var errMsg = {
        remitterName: '',
        remitAt: '',
        amount: '',
        partialAccountNum: '',
        remitterBank: '',
        checkinDate: '',
        remitterPhone: '',
        remitterEmail: '',
        remitRemark: '',
        userData: '',
        toString: function() {
            var msg = '';
            if (this.remitterName != '') {
                msg += this.remitterName + '<br>';
            };
            if (this.remitAt != '') {
                msg += this.remitAt + '<br>';
            };
            if (this.amount != '') {
                msg += this.amount + '<br>';
            };
            if (this.partialAccountNum != '') {
                msg += this.partialAccountNum + '<br>';
            };
            if (this.remitterBank != '') {
                msg += this.remitterBank + '<br>';
            };
            if (this.checkinDate != '') {
                msg += this.checkinDate + '<br>';
            };
            if (this.remitterPhone != '') {
                msg += this.remitterPhone + '<br>';
            };
            if (this.userData != '') {
                msg += this.userData + '<br>';
            };
            if (this.remitterEmail != '') {
                msg += this.remitterEmail + '<br>';
            };
            if (this.remitRemark != '') {
                msg += this.remitRemark + '<br>';
            };
            return msg;
        }
    };
    var $txtRemitterName = $('#txtRemitterName'),
        txtRemitterName = $txtRemitterName.val().trim();
    var $HiddenFieldRemitAt = $('#HiddenFieldRemitAt'),
        $txtRemitAt = $('#txtRemitAt'),
        txtRemitAt = $('#txtRemitAt').val().trim(),
        HiddenFieldRemitAt = $HiddenFieldRemitAt.val(txtRemitAt);
    var $txtAmount = $('#txtAmount'),
        txtAmount = $txtAmount.val().trim();
    var $txtPartialAccountNum = $('#txtPartialAccountNum'),
        txtPartialAccountNum = $txtPartialAccountNum.val().trim();
    var $txtRemitterBank = $('#txtRemitterBank'),
        txtRemitterBank = $txtRemitterBank.val().trim();
    var $HiddenFieldCheckinDate = $('#HiddenFieldCheckinDate'),
        $txtCheckinDate = $('#txtCheckinDate'),
        txtCheckinDate = $('#txtCheckinDate').val().trim(),
        HiddenFieldCheckinDate = $HiddenFieldCheckinDate.val(txtCheckinDate);
    var $txtRemitterPhone = $('#txtRemitterPhone'),
        txtRemitterPhone = $txtRemitterPhone.val().trim();
    var $txtRemitterEmail = $('#txtRemitterEmail'),
        txtRemitterEmail = $txtRemitterEmail.val().trim();
    var $txtRemitRemark = $('#txtRemitRemark'),
        txtRemitRemark = $txtRemitRemark.val().trim();

    /*檢驗開始*/
    /*檢驗-姓名*/
    if (txtRemitterName == '') {
        errMsg.remitterName = '<label for="txtRemitterName" class="alert-h">匯款姓名</label>此欄位必填';
        $txtRemitterName.addClass('alert-err');
    } else {
        $txtRemitterName.removeClass('alert-err');
    };
    /*檢驗-匯款日期*/
    if (txtRemitAt == '') {
        errMsg.remitAt = '<label for="txtRemitAt" class="alert-h">匯款日期</label>請選擇匯款日期';
        $txtRemitAt.addClass('alert-err');
    } else if (!dateValid(txtRemitAt)) {
        errMsg.remitAt = '<label for="txtRemitAt" class="alert-h">匯款日期</label>日期格式錯誤，如有疑問請重新整理介面';
        $txtRemitAt.addClass('alert-err');
    } else {
        $txtRemitAt.removeClass('alert-err');
    };
    /*檢驗-匯款金額*/
    if (txtAmount == '') {
        errMsg.amount = '<label for="txtAmount" class="alert-h">匯款金額</label>此欄位必填';
        $txtAmount.addClass('alert-err');
    } else if (txtAmount == 0) {
        errMsg.amount = '<label for="txtAmount" class="alert-h">匯款金額</label>請輸入不等於0的數字';
        $txtAmount.addClass('alert-err');
    } else if (!intValid(txtAmount)) {
        errMsg.amount = '<label for="txtAmount" class="alert-h">匯款金額</label>請勿輸入阿拉伯數字的文字';
        $txtAmount.addClass('alert-err');
    } else {
        $txtAmount.removeClass('alert-err');
    };
    /*檢驗-帳號末五碼*/
    if (txtPartialAccountNum == '') {
        errMsg.partialAccountNum = '<label for="txtPartialAccountNum" class="alert-h">帳號末五碼</label>此欄位必填';
        $txtPartialAccountNum.addClass('alert-err');
    } else if (txtPartialAccountNum.length != 5) {
        errMsg.partialAccountNum = '<label for="txtPartialAccountNum" class="alert-h">帳號末五碼</label>請輸入5個號碼';
        $txtPartialAccountNum.addClass('alert-err');
    } else if (!intValid(txtPartialAccountNum)) {
        errMsg.partialAccountNum = '<label for="txtPartialAccountNum" class="alert-h">帳號末五碼</label>請勿輸入阿拉伯數字的文字';
        $txtPartialAccountNum.addClass('alert-err');
    } else {
        $txtPartialAccountNum.removeClass('alert-err');
    };
    /*檢驗-匯款銀行*/
    if (txtRemitterBank == '') {
        errMsg.remitterBank = '<label for="txtRemitterBank" class="alert-h">匯款銀行</label>此欄位必填';
        $txtRemitterBank.addClass('alert-err');
    } else if (!txtValid(txtRemitterBank)) {
        errMsg.remitterBank = '<label for="txtRemitterBank" class="alert-h">匯款銀行</label>請勿輸入"<"、">"';
        $txtRemitterBank.addClass('alert-err');
    } else {
        $txtRemitterBank.removeClass('alert-err');
    };
    /*檢驗-入住日期*/
    if (txtCheckinDate == '') {
        errMsg.checkinDate = '<label for="txtCheckinDate" class="alert-h">入住日期</label>請選擇入住日期';
        $txtCheckinDate.addClass('alert-err');
    } else if (!dateValid(txtCheckinDate)) {
        errMsg.checkinDate = '<label for="txtCheckinDate" class="alert-h">入住日期</label>日期格式錯誤，如有疑問請重新整理介面';
        $txtCheckinDate.addClass('alert-err');
    } else {
        $txtCheckinDate.removeClass('alert-err');
    };
    /*檢驗-電話*/
    if (txtRemitterPhone == '') {
        errMsg.applicantPhone = '<label for="txtRemitterPhone" class="alert-h">連絡電話</label>此欄位必填';
        $txtRemitterPhone.addClass('alert-err');
    } else if (!telValid(txtRemitterPhone)) {
        errMsg.applicantPhone = '<label for="txtRemitterPhone" class="alert-h">連絡電話</label>請勿輸入數字、+、-以外的文字';
        $txtRemitterPhone.addClass('alert-err');
    } else {
        $txtRemitterPhone.removeClass('alert-err');
    };
    /*檢驗-電子郵件*/
    if (txtRemitterEmail == '') {
        errMsg.remitterEmail = '<label for="txtRemitterEmail" class="alert-h">電子郵件</label>此欄位必填';
        $txtRemitterEmail.addClass('alert-err');
    } else if (!emailValid(txtRemitterEmail)) {
        errMsg.remitterEmail = '<label for="txtRemitterEmail" class="alert-h">電子郵件</label>不符合郵件格式';
        $txtRemitterEmail.addClass('alert-err');
    } else {
        $txtRemitterEmail.removeClass('alert-err');
    };
    /*檢驗-其他說明*/
    if (txtRemitRemark != '' && !txtValid(txtRemitRemark)) {
        errMsg.remitRemark = '<label for="txtRemitRemark" class="alert-h">其他說明</label>請勿輸入"<"、">"';
        $txtRemitRemark.addClass('alert-err');
    } else {
        $txtRemitRemark.removeClass('alert-err');
    };
    /*檢驗-房型*/
    var roomStr = '';
    var i;
    var aIdx = $('.reserve-room-table tr').length;
    for (i = 0; i < aIdx; i++) {
        if ($('.room-type-select').eq(i).data('val') != '' && $('.qty-select').eq(i).data('val') != '') {
            if (i > 0 && roomStr.indexOf($('.room-type-select').eq(i).val().trim()) != -1) {
                errMsg.userData = '<label class="alert-h">所需房型</label>同一房型請勿重複選擇';
                $('.room-type-select').eq(i).addClass('alert-err');
                $('.qty-select').eq(i).addClass('alert-err');
                break;
            };
            roomStr += '; ' + $('.room-type-select').eq(i).val().trim() + ', ' + $('.room-type-select').eq(i).data('val') + ', ' + $('.qty-select').eq(i).data('val');
        } else {
            errMsg.userData = '<label class="alert-h">所需房型</label>請檢查已正確選擇房型、且數量不為0';
            $('.room-type-select').eq(i).addClass('alert-err');
            $('.qty-select').eq(i).addClass('alert-err');
            break;
        };
        $('.room-type-select').eq(i).removeClass('alert-err');
        $('.qty-select').eq(i).removeClass('alert-err');
    };

    //如果錯誤訊息不是空的，顯示錯誤訊息
    if (errMsg.toString() != '') {
        $('.alert-block').css('display', 'block').html(errMsg.toString());
        console.log('f');
        return false;
    } else {
        // alert(roomStr.replace(';', ''));
        $('#HiddenFieldUserData').val(roomStr.replace(';', ''));
        console.log('t');
        return true;
    };

};

/*檢查數字*/
function intValid(txt) {
    var reg = /^[0-9]+$/;
    if (reg.test(txt)) {
        return true;
    } else {
        return false;
    }
}; //end function

/*檢查電話*/
function telValid(tel) {
    var reg = /^[0-9+-]+$/;
    if (reg.test(tel)) {
        return true;
    } else {
        return false;
    }
}; //end function

/* 檢查email格式 */
function emailValid(email) {
    var reg = /^[^\s]+@[^\s]+\.[^\s]{2,3}$/;
    if (reg.test(email)) {
        return true;
    } else {
        return false;
    }
}; //end function

/*檢查惡意*/
function txtValid(txt) {
    var reg = /^[^<>]+$/;
    if (reg.test(txt)) {
        return true;
    } else {
        return false;
    }
}; //end function

/*驗證日期yyyy/mm/dd*/
function dateValid(date) {
    var reg = /^([0-9]{4})+\/([0-9]{2})+\/([0-9]{2})$/;
    if (reg.test(date)) {
        return true;
    } else {
        return false;
    }
}; //end function