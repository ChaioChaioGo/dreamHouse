window.addEventListener('DOMContentLoaded', function(e) {
    // DOM Tree 載完
    // console.log('onDOMContentLoaded');

    // 創建 2a獨有的捲動按鈕
    var link = document.createElement('a');
    link.id = 'scrollWindowDown';
    link.className = 'master-scroll-btn';
    link.href = 'javascript:;';
    document.getElementById('header').appendChild(link);

    // v2 重組 master 元件
    // header 元件
    var headerContent = {
        'header': [
            'bnbLogo',
            'bnbName',
            'bnbSlogan',
            'scrollWindowDown', // 在上面的程式中動態產生
            'bnbPcBanner'
        ]
    };
    // footer 元件
    var footerContent = {
        'footerInner': [
            'footerLegalHouse',
            'footerBnbName',
            'bookingTelDiv',
            'footerFansClub',
            'footerLineFriend',
            'footerInforDiv',
            'footerCopyright',
            'socialLinkList'
        ],
    };
    // 欲刪除的元件
    var mainNone = [
        'footerLogo',
        'liBookingTel1',
        'footerWebGuide'
    ];

    // 組裝 DOM
    Tools.setDOM('header', headerContent);
    Tools.setDOM('footerInner', footerContent);
    // 刪除 DOM
    Tools.delDOM(mainNone);


    init2a();

});

var init2a = function() {
    var $scrollWinDown = $('#scrollWindowDown');
    var $body = $('body, html');
    initKBE();
    // banner動畫設定
    function initKBE() {
        var aniVal = parseFloat(document.getElementById('HiddenFieldVersionParam').value) || 0;
        if (aniVal !== 0) {
            var kbe_Param = [{ /*val =  0*/
                    f1: 1,
                    f2: 1,
                    p1: 'center',
                    p2: 'center'
                },
                { /*val =  1*/
                    f1: 1,
                    f2: 1.15,
                    p1: 'center',
                    p2: 'center'
                },
                { /*val =  2*/
                    f1: 1.15,
                    f2: 1,
                    p1: 'center',
                    p2: 'center'
                },
                { /*val =  3*/
                    f1: 1.15,
                    f2: 1,
                    p1: 'center',
                    p2: 'top'
                },
                { /*val =  4*/
                    f1: 1.15,
                    f2: 1,
                    p1: 'center',
                    p2: 'bottom'
                },
                { /*val =  5*/
                    f1: 1,
                    f2: 1.15,
                    p1: 'center',
                    p2: 'top'
                },
                { /*val =  6*/
                    f1: 1,
                    f2: 1.15,
                    p1: 'center',
                    p2: 'bottom'
                },
                { /*val =  7*/
                    f1: 1.15,
                    f2: 1,
                    p1: 'left',
                    p2: 'top'
                },
                { /*val =  8*/
                    f1: 1.15,
                    f2: 1,
                    p1: 'left',
                    p2: 'bottom'
                },
                { /*val =  9*/
                    f1: 1,
                    f2: 1.15,
                    p1: 'left',
                    p2: 'top'
                },
                { /*val = 10*/
                    f1: 1,
                    f2: 1.15,
                    p1: 'left',
                    p2: 'bottom'
                },
                { /*val = 11*/
                    f1: 1.15,
                    f2: 1,
                    p1: 'right',
                    p2: 'top'
                },
                { /*val = 12*/
                    f1: 1.15,
                    f2: 1,
                    p1: 'right',
                    p2: 'bottom'
                },
                { /*val = 13*/
                    f1: 1,
                    f2: 1.15,
                    p1: 'right',
                    p2: 'top'
                },
                { /*val = 14*/
                    f1: 1,
                    f2: 1.15,
                    p1: 'right',
                    p2: 'bottom'
                }
            ];
            var f1 = kbe_Param[aniVal].f1;
            var f2 = kbe_Param[aniVal].f2;
            var p1 = kbe_Param[aniVal].p1;
            var p2 = kbe_Param[aniVal].p2;
            var time = 9;
            var sheet = document.createElement('style');
            var anim = '@-webkit-keyframes burnseffect {\
                            0% {\
                                -webkit-transform:scale(' + f1 + ');\
                                transform:scale(' + f1 + ');\
                            }\
                            100% {\
                                -webkit-transform:scale(' + f2 + ');\
                                transform:scale(' + f2 + ');\
                            }\
                        }\
                        @keyframes burnseffect {\
                            0% {\
                                -webkit-transform:scale(' + f1 + ');\
                                transform:scale(' + f1 + ');\
                            }\
                            100% {\
                                -webkit-transform:scale(' + f2 + ');\
                                transform:scale(' + f2 + ');\
                            }\
                        }\
                        .master-pc-banner, .master-mob-banner {\
                            -webkit-transform-origin:' + p1 + ' ' + p2 + ';\
                            -ms-transform-origin:' + p1 + ' ' + p2 + ';\
                            transform-origin:' + p1 + ' ' + p2 + ';\
                            -webkit-animation: burnseffect ' + time + 's linear forwards;\
                            -moz-animation: burnseffect ' + time + 's linear forwards;\
                            animation: burnseffect ' + time + 's linear forwards;\
                        }';

            document.head.appendChild(sheet);
            sheet.appendChild(document.createTextNode(anim));
            document.head.appendChild(sheet);
        };
    };

    function toMain() {
        console.log('a2');
        // console.log(window.innerHeight);
        $body.stop(true, false).animate({
            scrollTop: window.innerHeight - 120
        }, 1000, 'easeOutCubic');
    };
    $scrollWinDown.on('click', toMain);
};

$(function() {
    console.log('jq load');
    var $win = $(window);
    var $body = $('body, html');
    var $mainDiv = $('#main');
    var $menu = $('#masterMenu');
    var $nav = $('.swiper-outter-container');
    var $bar1 = $('.bar1'),
        $bar2 = $('.bar2'),
        $bar3 = $('.bar3');
    var $goTop = $('#pageToTop');
    var swiper, recover = false;
    var $swiWrapper = $('.swiper-wrapper');
    var hiddenSlide = $('.swiper-slide.hidden').length + 1;
    var $gooTransBar = $('.google-language');
    var $bnbPcBanner = $('#bnbPcBanner');

    function initSwiper() {
        if (window.innerWidth > 767) {
            //先取得各個nav連接相加的總長
            var swtw = 0; //swiper Wrapper total width
            $('.swiper-slide').each(function(index, el) {
                swtw = $(el).find('span').width() + 40 + swtw;
            });
            //這裡要算出整個slider的總長
            $swiWrapper.css('width', swtw + 'px');
            //呼叫 swiper
            createSwiper();
            swiper.update(true);
        };
        setSwiperCss();
    }; //end initSwiper

    function resizeSwiper() {
        if (window.innerWidth > 767) {
            //先取得各個nav連接相加的總長
            var swtw = 0; //swiper Wrapper total width
            $('.swiper-slide').each(function(index, el) {
                swtw = $(el).find('span').width() + 40 + swtw;
            });
            if ($swiWrapper.width() !== swtw) {
                $swiWrapper.css('width', swtw + 'px');
            };
            //呼叫 swiper
            if (typeof swiper === 'object') {
                swiper.update(true);
                swiper.slideTo(0, 0);
                if (recover) {
                    createSwiper();
                    recover = false;
                };
            } else if (typeof swiper === 'undefined') {
                createSwiper();
            };
            setSwiperCss();
        } else {
            $swiWrapper.css('width', '100%');
            if (typeof swiper === 'object') {
                swiper.destroy(false, true);
                recover = true;
            };
        };
    }; //end resizeSwiper
    function setSwiperCss() {
        $('.swiper-slide').css('opacity', 1);
        $('.swiper-button-prev').css('opacity', 1);
        $('.swiper-button-next').css('opacity', 1);
    };

    function createSwiper() {
        swiper = new Swiper('.swiper-container', {
            slidesPerView: 'auto',
            calculateHeight: true,
            preventClicks: false,
            prevButton: '.swiper-button-prev',
            nextButton: '.swiper-button-next',
            grabCursor: true
        });
        var idx = findActiveIdx();
        if (idx > 1) {
            idx -= 1
        };
        swiper.slideTo(idx, 0, false);
    };

    function findActiveIdx() { // 找 active 的 idx
        return $('.swiper-slide').index($('.swiper-slide.active'));
    };

    function menuAnimate() {
        if (document.querySelector('.show-menu') === null) {
            $bar1.transition({
                y: 17
            }, 300).transition({
                rotate: '45deg'
            }, 300);
            $bar2.transition({
                y: 14
            }, 200).transition({
                opacity: 0
            }, 200);
            $bar3.transition({
                y: 11
            }, 300).transition({
                rotate: '-45deg'
            }, 300);
        } else {
            $bar1.transition({
                rotate: 0
            }, 300).transition({
                y: 9
            }, 300);
            $bar2.transition({
                opacity: 0
            }, 200).transition({
                opacity: 1
            }, 200);
            $bar3.transition({
                rotate: 0
            }, 300).transition({
                y: 19
            }, 300);
        };
        $body.toggleClass('trans-main');
        $mainDiv.toggleClass('trans-main-on');
        $menu.toggleClass('show-menu');
        $nav.toggleClass('show-menu');
    };

    function toTop() {
        $body.stop(true, false).animate({
            scrollTop: 0
        }, 1000, 'easeOutCubic');
        $goTop.css({
            display: 'none'
        });
    };

    window.addEventListener('load', function(e) {
        // 網頁所有的東西都載完
        console.log('load');
        initSwiper();
        $win.on('orientationchange resize', resizeSwiper);
    });

    $win.on('scroll', function() {
        /*2017-05-05 新增google翻譯的卷軸觸發啟閉*/
        if ($(this).scrollTop() > 0) {
            $goTop.css({
                display: 'block'
            });
            $gooTransBar.addClass('hidden');
        } else {
            $goTop.css({
                display: 'none'
            });
            $gooTransBar.removeClass('hidden');
        };
    });

    (function() {
        if ($win.scrollTop() > 0) {
            $goTop.css({
                display: 'block'
            });
            $gooTransBar.addClass('hidden');
        } else {
            $goTop.css({
                display: 'none'
            });
            $gooTransBar.removeClass('hidden');
        };
    })();


    $menu.on('click', menuAnimate);
    $goTop.on('click', toTop);

    // 加上電話連結(手機)
    if (document.getElementById('bookingTelNum1') && Tools.isMobile) {
        var phoneTxt1 = document.getElementById('bookingTelNum1').textContent;
        document.getElementById('bookingTelNum1').innerHTML = '<a href="tel:' + phoneTxt1 + '">' + phoneTxt1 + '</a>';
    };
    if (document.getElementById('bookingTelNum2') && Tools.isMobile) {
        var phoneTxt2 = document.getElementById('bookingTelNum2').textContent;
        document.getElementById('bookingTelNum2').innerHTML = '<a href="tel:' + phoneTxt2 + '">' + phoneTxt2 + '</a>';
    };

    // 新增的臉書私訊外掛，如果有掛上這個外掛，要另外在body加上.has-chat-plugin
    if (document.getElementById('HiddenFieldFbChatPlugin') && document.getElementById('HiddenFieldFbChatPlugin').value === 'true') {
        $body.addClass('has-chat-plugin');
    };

});

// 捲動用 fn
function mobileSetWinowTopPostion(p_el, m_el) {
    var targetPElOffsetTop = document.getElementById(p_el).offsetTop;
    var targetMElOffsetTop = (m_el) ? document.getElementById(m_el).offsetTop : 0;
    var pBase = document.getElementById('navContainer').clientHeight;
    var mBase = document.getElementById('header').clientHeight;

    // console.log('main targetPElOffsetTop', targetPElOffsetTop);
    // console.log('____ targetMElOffsetTop', targetMElOffsetTop);

    // console.log('header clientHeight', document.getElementById('header').clientHeight);
    // console.log('navContainer clientHeight', document.getElementById('navContainer').clientHeight);

    // pc navContainer
    // mobile header 
    // 把視窗拉到定位點
    if (targetPElOffsetTop > 0) {
        window.scrollTo(0, targetPElOffsetTop - pBase);
    } else {
        window.scrollTo(0, targetMElOffsetTop - mBase);
    };
};

$(function() {
    var $el = $('.toggleTrans');
    var $el2 = $('.goog-te-menu-frame');
    var $win = $(window);

    $el.click(function() {
        var $this = $(this);
        var offsetT = $this.offset().top;
        var offsetB = $this.offset().bottom;
        var offsetL = $this.offset().left;
        var h = $this.outerHeight();
        var scrollTop = $win.scrollTop();
        // 2a
        if (window.innerWidth > 767) {
            $('.goog-te-menu-frame').eq(0).toggle().css({
                top: offsetT - scrollTop + h,
                bottom: 'none',
                left: offsetL
            });
        } else {
            $('.goog-te-menu-frame').eq(0).toggle().css({
                top: 'none',
                bottom: '85px',
                left: offsetL
            });
        };

    });
});